package shared;

public class Queue<T> {
	private Node<T> first;
    private Node<T> last;
    private int size; //TODO make volatile?

    private static class Node<Item> {
        private Item item;
        private Node<Item> next;
    }

    /**
     * Returns true if this queue is empty.
     *
     * @return <tt>true</tt> if this queue is empty; <tt>false</tt> otherwise
     */
    public synchronized boolean isEmpty() {
        return first == null;
    }

    /**
     * Returns the number of items in this queue.
     *
     * @return the number of items in this queue
     */
    public synchronized int size() {
        return size;
    }

    /**
     * Adds the item to this queue.
     *
     * @param  item the item to add
     */
    public synchronized void add(T item) {
        Node<T> oldlast = last;
        last = new Node<T>();
        last.item = item;
        last.next = null;
        if (isEmpty())
        	first = last;
        else
        	oldlast.next = last;
        size++;
    }

    /**
     * Removes and returns the item on this queue that was least recently added.
     *
     * @return the item on this queue that was least recently added, or null if the queue is empty
     */
    public synchronized T get() {
        if (isEmpty())
        	return null;
        T item = first.item;
        first = first.next;
        size--;
        if (isEmpty())
        	last = null;   // to avoid loitering
        return item;
    }
    
    public static void main(String[] args) {
    	Queue<String> q = new Queue<String>();
    	q.add("test1");
    	q.add("test2");
    	System.out.println(q.get());
    	q.add("test3");
    	System.out.println(q.get());
    	System.out.println(q.get());
    }
}
