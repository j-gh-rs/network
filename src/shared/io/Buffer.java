package shared.io;

import java.math.BigInteger;
import java.nio.ByteBuffer;

public class Buffer {

	protected byte data[];
	private int position;
	private int bitPosition;
	private int save;

	public Buffer() {
		this(500);
	}

	public Buffer(int length) {
		this(new byte[length]);
	}

	public Buffer(byte[] data) {
		this.data = data;
		position = 0;
		save = 0;
	}

	public void writeOp(int value) {
		writeInt8(value);
	}

	public void writeInt8(int value) {
		data[position++] = (byte)value;
	}

	public void writeInt16(int value) {
		data[position++] = (byte) (value >> 8);
		data[position++] = (byte) value;
	}

	public void writeInt24(int value) {
		data[position++] = (byte) (value >> 16);
		data[position++] = (byte) (value >> 8);
		data[position++] = (byte) value;
	}

	public void writeInt32(int value) {
		data[position++] = (byte) (value >> 24);
		data[position++] = (byte) (value >> 16);
		data[position++] = (byte) (value >> 8);
		data[position++] = (byte) value;
	}

	public void writeInt64(long value) {
		data[position++] = (byte) (value >> 56);
		data[position++] = (byte) (value >> 48);
		data[position++] = (byte) (value >> 40);
		data[position++] = (byte) (value >> 32);
		data[position++] = (byte) (value >> 24);
		data[position++] = (byte) (value >> 16);
		data[position++] = (byte) (value >> 8);
		data[position++] = (byte) value;
	}
	
	public void writeUInt8(int value) {
		writeInt8(value);
	}

	public void writeUInt16(int value) {
		writeInt16(value);
	}

	public void writeUInt24(int value) {
		writeInt24(value);
	}

	public void writeUInt32(int value) {
		writeInt32(value);
	}

	public void writeUInt64(long value) {
		writeInt64(value);
	}

	public byte readInt8() {
		return data[position++];
	}

	public int readInt16() {
		position += 2;
		return ((data[position - 2] << 8)
				| data[position - 1] & 0xFF);
	}

	public int readInt24() {
		position += 3;
		return (data[position - 3]) << 16
				| (data[position - 2] & 0xFF) << 8
				| (data[position - 1] & 0xFF);
	}

	public int readInt32() {
		position += 4;
		return (data[position - 4]) << 24
				| (data[position - 3] & 0xFF) << 16
				| (data[position - 2] & 0xFF) << 8
				| (data[position - 1] & 0xFF);
	}

	public long readInt64() {
		position += 8;
		return (data[position - 8]) << 56
				| (data[position - 7] & 0xFF) << 48
				| (data[position - 6] & 0xFF) << 40
				| (data[position - 5] & 0xFF) << 32
				| (data[position - 4] & 0xFF) << 24
				| (data[position - 3] & 0xFF) << 16
				| (data[position - 2] & 0xFF) << 8
				| (data[position - 1] & 0xFF);
	}

	public int readUInt8() {
		return (data[position++] & 0xFF);
	}

	public int readUInt16() {
		position += 2;
		return (data[position - 2] & 0xFF) << 8
				| (data[position - 1] & 0xFF);
	}
	
	public int readUInt24() {
		position += 3;
		return (data[position - 3] & 0xFF) << 16
				| (data[position - 2] & 0xFF) << 8
				| (data[position - 1] & 0xFF);
	}

	public int readUInt32() {
		position += 4;
		return (data[position - 4] & 0xFF) << 24
				| (data[position - 3] & 0xFF) << 16
				| (data[position - 2] & 0xFF) << 8
				| (data[position - 1] & 0xFF);
	}

	public long readUInt64() {
		position += 8;
		return (data[position - 8] & 0xFFL) << 56
				| (data[position - 7] & 0xFFL) << 48
				| (data[position - 6] & 0xFFL) << 40
				| (data[position - 5] & 0xFFL) << 32
				| (data[position - 4] & 0xFFL) << 24
				| (data[position - 3] & 0xFFL) << 16
				| (data[position - 2] & 0xFFL) << 8
				| (data[position - 1] & 0xFFL);
	}

//	public byte[] readBytes(byte endByte) {
//		int startPosition = position;
//		while (data[position++] != endByte);
//		byte[] b = new byte[position - startPosition - 1];
//		System.arraycopy(data, startPosition, b, 0, b.length);
//		return b;
//	}

	public void write(byte[] value) {
		write(value, 0, value.length);
	}

	public void read(byte[] value) {
		read(value, 0, value.length);
	}

	public void write(byte[] value, int offset, int length) {
		System.arraycopy(value, offset, data, position, length);
		position += length;
	}

	public void read(byte[] value, int offset, int length) {
		System.arraycopy(data, position, value, offset, length);
		position += length;
	}

	public void write(Buffer value) {
		write(value.data, 0, value.position);
	}

	public void writeString(String text) {
		write(text.getBytes());
		writeInt8(10);
	}

	public String readString(byte endByte) {
		int startPosition = position;
		while(data[position++] != endByte);
		return new String(data, startPosition, position - startPosition - 1);
	}
	
	public String readString() {
		return readString((byte) 10);
	}

	public void save() {
		save = position;
	}
	
	public int getSave() {
		return save;
	}

	public void load() {
		position = save;
	}

	public void reset() {
		position = 0;
	}

	public int position() {
		return position;
	}
	
	public int bitPosition() {
		return bitPosition;
	}

	public int length() {
		return data.length;
	}

	public boolean isEmpty() {
		return position == 0;
	}
	
	public int remaining() {
		return data.length - position;
	}

	public byte[] data() {
		return data;
	}

	public void seek(int position) {
		this.position = position;
	}

	public void skip(int i) {
		position += i;
	}

	public void initBitAccess() {
		bitPosition = position * 8;
	}

	public void finishBitAccess() {
		position = (bitPosition + 7) / 8;
	}

	public void writeBits(int amount, int value) {
		int byteOffset = bitPosition >> 3;
		int bitOffset = 8 - (bitPosition & 7);
		bitPosition += amount;
		
		if (amount <= bitOffset) {
			data[byteOffset] |= (value & BIT_MASKS[amount]) << bitOffset-amount;
		} else {
			data[byteOffset++] |= (value >>> amount - bitOffset) & BIT_MASKS[bitOffset];
			amount -= bitOffset;
			while(amount > 8) {
				data[byteOffset++] = (byte)(value >>> (amount - 8));
				amount -= 8;
			}
			data[byteOffset] |= (value & BIT_MASKS[amount]) << 8 - amount;
		}
	}

	public int readBits(int amount) {
		int byteOffset = bitPosition >> 3;
		int bitOffset = 8 - (bitPosition & 7);
		bitPosition += amount;
		
		if (amount <= bitOffset)
			return (data[byteOffset] & (BIT_MASKS[bitOffset] ^ BIT_MASKS[bitOffset-amount])) >>> bitOffset-amount;
		
		int value = data[byteOffset++] & BIT_MASKS[bitOffset];
		amount -= bitOffset;
		while(amount > 8) {
			value <<= 8;
			value |= data[byteOffset++] & 0xFF;
			amount -= 8;
		}
		value <<= amount;
		value |= ((data[byteOffset] >>> 8-amount) & BIT_MASKS[amount]);
		
		return value;
	}
	
	public void writePacketLength() {
		data[save - 1] = (byte)(position - save);
	}

	public static final int BIT_MASKS[] = {	0x00000000, 0x00000001, 0x00000003, 0x00000007,
										0x0000000F, 0x0000001F, 0x0000003F, 0x0000007F,
										0x000000FF, 0x000001FF, 0x000003FF, 0x000007FF,
										0x00000FFF, 0x00001FFF, 0x00003FFF, 0x00007FFF,
										0x0000FFFF, 0x0001FFFF, 0x0003FFFF, 0x0007FFFF,
										0x000FFFFF, 0x001FFFFF, 0x003FFFFF, 0x007FFFFF,
										0x00FFFFFF, 0x01FFFFFF, 0x03FFFFFF, 0x07FFFFFF,
										0x0FFFFFFF, 0x1FFFFFFF, 0x3FFFFFFF, 0x7FFFFFFF,
										0xFFFFFFFF };
	
	
	
	
	
	
	
	
	
	
	
//	
//	
//	
//	
//	
//	
//	
//
//	
//	
//	
//	/****************************************** CLIENT PART ********************************************************/
//
//	public void method400(int i) {
//		data[position++] = (byte)i;
//		data[position++] = (byte)(i >> 8);
//	}
//
//	public void method403(int j) {
//		data[position++] = (byte)j;
//		data[position++] = (byte)(j >> 8);
//		data[position++] = (byte)(j >> 16);
//		data[position++] = (byte)(j >> 24);
//	}
//
//	public int method421() {
//		int i = data[position] & 0xff;
//		if(i < 128)
//			return readUInt8() - 64;
//		else
//			return readUInt16() - 49152;
//	}
//
//	public int method422() {
//		int i = data[position] & 0xff;
//		if(i < 128)
//			return readUInt8();
//		else
//			return readUInt16() - 32768;
//	}
//
//	public void doKeys() {
//		int i = position;
//		position = 0;
//		byte abyte0[] = new byte[i];
//		read(abyte0, 0, i);
//		BigInteger biginteger2 = new BigInteger(abyte0);
//		BigInteger biginteger3 = biginteger2/*.modPow(biginteger, biginteger1)*/;
//		byte abyte1[] = biginteger3.toByteArray();
//		position = 0;
//		writeInt8(abyte1.length);
//		write(abyte1);
//	}
//
//	public void method424(int i) {
//		data[position++] = (byte)(-i);
//	}
//
//	public void method425(int j) {
//		data[position++] = (byte)(128 - j);
//	}
//
//	public int method426() {
//		return data[position++] - 128 & 0xff;
//	}
//
//	public int method427() {
//		return -data[position++] & 0xff;
//	}
//
//	public int method428() {
//		return 128 - data[position++] & 0xff;
//	}
//
//	public byte method429() {
//		return (byte)(-data[position++]);
//	}
//
//	public byte method430() {
//		return (byte)(128 - data[position++]);
//	}
//
//	public void method431(int i) {
//		data[position++] = (byte)i;
//		data[position++] = (byte)(i >> 8);
//	}
//
//	public void method432(int j) {
//		data[position++] = (byte)(j >> 8);
//		data[position++] = (byte)(j + 128);
//	}
//
//	public void method433(int j) {
//		data[position++] = (byte)(j + 128);
//		data[position++] = (byte)(j >> 8);
//	}
//
//	public int method434() {
//		position += 2;
//		return ((data[position - 1] & 0xff) << 8) + (data[position - 2] & 0xff);
//	}
//
//	public int method435() {
//		position += 2;
//		return ((data[position - 2] & 0xff) << 8) + (data[position - 1] - 128 & 0xff);
//	}
//
//	public int method436() {
//		position += 2;
//		return ((data[position - 1] & 0xff) << 8) + (data[position - 2] - 128 & 0xff);
//	}
//
//	public int method437() {
//		position += 2;
//		int j = ((data[position - 1] & 0xff) << 8) + (data[position - 2] & 0xff);
//		if(j > 32767)
//			j -= 0x10000;
//		return j;
//	}
//
//	public int method438() {
//		position += 2;
//		int j = ((data[position - 1] & 0xff) << 8) + (data[position - 2] - 128 & 0xff);
//		if(j > 32767)
//			j -= 0x10000;
//		return j;
//	}
//
//	public int method439() {
//			position += 4;
//			return ((data[position - 2] & 0xff) << 24) + ((data[position - 1] & 0xff) << 16) + ((data[position - 4] & 0xff) << 8) + (data[position - 3] & 0xff);
//	}
//
//	public int method440() {
//		position += 4;
//		return ((data[position - 3] & 0xff) << 24) + ((data[position - 4] & 0xff) << 16) + ((data[position - 1] & 0xff) << 8) + (data[position - 2] & 0xff);
//	}
//
//	public void method441(int i, byte abyte0[], int j) {
//		for(int k = (i + j) - 1; k >= i; k--)
//			data[position++] = (byte)(abyte0[k] + 128);
//
//	}
//
//	public void method442(int i, int j, byte abyte0[]) {
//		for(int k = (j + i) - 1; k >= j; k--)
//			abyte0[k] = data[position++];
//
//	}
//	
//	
//	
//	
//	
//	
//	
//	
//	
//	
//	
//	
//	
//	
//	
//	
//	
//	
//	
//	
//	
//	
//	/***********************************************SERVER PART*************************************************/
//
//	/**
//	 * An enum whose values represent the possible order in which bytes are
//	 * written in a multiple-byte value. Also known as "endianness".
//	 * 
//	 * @author blakeman8192
//	 */
//	public static enum ByteOrder {
//		LITTLE, BIG, MIDDLE, INVERSE_MIDDLE
//	}
//
//	/**
//	 * An enum whose values represent the possible custom RuneScape value types.
//	 * Type A is to add 128 to the value, type C is to invert the value, and
//	 * type S is to subtract the value from 128. Of course, STANDARD is just the
//	 * normal data value.
//	 * 
//	 * @author blakeman8192
//	 */
//	public static enum ValueType {
//		STANDARD, A, C, S
//	}
//
//	/**
//	 * An enum whose values represent the current type of access to a
//	 * StreamBuffer. BYTE_ACCESS is for reading/writing bytes, BIT_ACCESS is for
//	 * reading/writing bits.
//	 * 
//	 * @author blakeman8192
//	 */
//	public static enum AccessType {
//		BYTE_ACCESS, BIT_ACCESS
//	}
//
//	/**
//	 * Bit masks.
//	 */
//	public static final int[] BIT_MASK = { 0, 0x1, 0x3, 0x7, 0xf, 0x1f, 0x3f,
//			0x7f, 0xff, 0x1ff, 0x3ff, 0x7ff, 0xfff, 0x1fff, 0x3fff, 0x7fff,
//			0xffff, 0x1ffff, 0x3ffff, 0x7ffff, 0xfffff, 0x1fffff, 0x3fffff,
//			0x7fffff, 0xffffff, 0x1ffffff, 0x3ffffff, 0x7ffffff, 0xfffffff,
//			0x1fffffff, 0x3fffffff, 0x7fffffff, -1 };
//
//	/** The current AccessType of the buffer. */
//	private AccessType accessType = AccessType.BYTE_ACCESS;
//
//	/**
//	 * Creates a new InBuffer.
//	 * 
//	 * @param data
//	 *            the data
//	 * @return a new InBuffer
//	 */
//	public static final InBuffer newInBuffer(ByteBuffer data) {
//		return new InBuffer(data);
//	}
//
//	/**
//	 * Creates a new OutBuffer.
//	 * 
//	 * @param size
//	 *            the size
//	 * @return a new OutBuffer
//	 */
//	public static final OutBuffer newOutBuffer(int size) {
//		return new OutBuffer(size);
//	}
//
//	/**
//	 * Handles the internal switching of the access type.
//	 * 
//	 * @param type
//	 *            the new access type
//	 */
//	void switchAccessType(AccessType type) {}
//
//	/**
//	 * Sets the AccessType of this StreamBuffer.
//	 * 
//	 * @param accessType
//	 *            the new AccessType
//	 */
//	public void setAccessType(AccessType accessType) {
//		this.accessType = accessType;
//		switchAccessType(accessType);
//	}
//
//	/**
//	 * Gets the AccessType of this StreamBuffer.
//	 * 
//	 * @return the current AccessType
//	 */
//	public AccessType getAccessType() {
//		return accessType;
//	}
//
//	/**
//	 * Sets the bit position.
//	 * 
//	 * @param bitPosition
//	 *            the new bit position
//	 */
//	public void setBitPosition(int bitPosition) {
//		this.bitPosition = bitPosition;
//	}
//
//	/**
//	 * Gets the current bit position.
//	 * 
//	 * @return the bit position
//	 */
//	public int getBitPosition() {
//		return bitPosition;
//	}
//
//	/**
//	 * A StreamBuffer used to read incoming data.
//	 * 
//	 * @author blakeman8192
//	 */
//	public static final class InBuffer extends Buffer {
//
//		/** The internal buffer. */
//		private ByteBuffer buffer;
//
//		/**
//		 * Creates a new InBuffer.
//		 * 
//		 * @param buffer
//		 *            the buffer
//		 */
//		private InBuffer(ByteBuffer buffer) {
//			this.buffer = buffer;
//		}
//
//		@Override
//		void switchAccessType(AccessType type) {
//			if (type == AccessType.BIT_ACCESS) {
//				throw new UnsupportedOperationException(
//						"Reading bits is not implemented!");
//			}
//		}
//
//		/**
//		 * Reads a value as a byte.
//		 * 
//		 * @param signed
//		 *            the signedness
//		 * @param type
//		 *            the value type
//		 * @return the value
//		 */
//		public int readByte(boolean signed, ValueType type) {
//			int value = buffer.get();
//			switch (type) {
//			case A:
//				value = value - 128;
//				break;
//			case C:
//				value = -value;
//				break;
//			case S:
//				value = 128 - value;
//				break;
//			}
//			return signed ? value : value & 0xff;
//		}
//
//		/**
//		 * Reads a standard signed byte.
//		 * 
//		 * @return the value
//		 */
//		public int readByte() {
//			return readByte(true, ValueType.STANDARD);
//		}
//
//		/**
//		 * Reads a standard byte.
//		 * 
//		 * @param signed
//		 *            the signedness
//		 * @return the value
//		 */
//		public int readByte(boolean signed) {
//			return readByte(signed, ValueType.STANDARD);
//		}
//
//		/**
//		 * Reads a signed byte.
//		 * 
//		 * @param type
//		 *            the value type
//		 * @return the value
//		 */
//		public int readByte(ValueType type) {
//			return readByte(true, type);
//		}
//
//		/**
//		 * Reads a short value.
//		 * 
//		 * @param signed
//		 *            the signedness
//		 * @param type
//		 *            the value type
//		 * @param order
//		 *            the byte order
//		 * @return the value
//		 */
//		public int readShort(boolean signed, ValueType type, ByteOrder order) {
//			int value = 0;
//			switch (order) {
//			case BIG:
//				value |= readByte(false) << 8;
//				value |= readByte(false, type);
//				break;
//			case MIDDLE:
//				throw new UnsupportedOperationException(
//						"Middle-endian short is impossible!");
//			case INVERSE_MIDDLE:
//				throw new UnsupportedOperationException(
//						"Inverse-middle-endian short is impossible!");
//			case LITTLE:
//				value |= readByte(false, type);
//				value |= readByte(false) << 8;
//				break;
//			}
//			return signed ? value : value & 0xffff;
//		}
//
//		/**
//		 * Reads a standard signed big-endian short.
//		 * 
//		 * @return the value
//		 */
//		public int readShort() {
//			return readShort(true, ValueType.STANDARD, ByteOrder.BIG);
//		}
//
//		/**
//		 * Reads a standard big-endian short.
//		 * 
//		 * @param signed
//		 *            the signedness
//		 * @return the value
//		 */
//		public int readShort(boolean signed) {
//			return readShort(signed, ValueType.STANDARD, ByteOrder.BIG);
//		}
//
//		/**
//		 * Reads a signed big-endian short.
//		 * 
//		 * @param type
//		 *            the value type
//		 * @return the value
//		 */
//		public int readShort(ValueType type) {
//			return readShort(true, type, ByteOrder.BIG);
//		}
//
//		/**
//		 * Reads a big-endian short.
//		 * 
//		 * @param signed
//		 *            the signedness
//		 * @param type
//		 *            the value type
//		 * @return the value
//		 */
//		public int readShort(boolean signed, ValueType type) {
//			return readShort(signed, type, ByteOrder.BIG);
//		}
//
//		/**
//		 * Reads a signed standard short.
//		 * 
//		 * @param order
//		 *            the byte order
//		 * @return the value
//		 */
//		public int readShort(ByteOrder order) {
//			return readShort(true, ValueType.STANDARD, order);
//		}
//
//		/**
//		 * Reads a standard short.
//		 * 
//		 * @param signed
//		 *            the signedness
//		 * @param order
//		 *            the byte order
//		 * @return the value
//		 */
//		public int readShort(boolean signed, ByteOrder order) {
//			return readShort(signed, ValueType.STANDARD, order);
//		}
//
//		/**
//		 * Reads a signed short.
//		 * 
//		 * @param type
//		 *            the value type
//		 * @param order
//		 *            the byte order
//		 * @return the value
//		 */
//		public int readShort(ValueType type, ByteOrder order) {
//			return readShort(true, type, order);
//		}
//
//		/**
//		 * Reads an integer.
//		 * 
//		 * @param signed
//		 *            the signedness
//		 * @param type
//		 *            the value type
//		 * @param order
//		 *            the byte order
//		 * @return the value
//		 */
//		public long readInt(boolean signed, ValueType type, ByteOrder order) {
//			long value = 0;
//			switch (order) {
//			case BIG:
//				value |= readByte(false) << 24;
//				value |= readByte(false) << 16;
//				value |= readByte(false) << 8;
//				value |= readByte(false, type);
//				break;
//			case MIDDLE:
//				value |= readByte(false) << 8;
//				value |= readByte(false, type);
//				value |= readByte(false) << 24;
//				value |= readByte(false) << 16;
//				break;
//			case INVERSE_MIDDLE:
//				value |= readByte(false) << 16;
//				value |= readByte(false) << 24;
//				value |= readByte(false, type);
//				value |= readByte(false) << 8;
//				break;
//			case LITTLE:
//				value |= readByte(false, type);
//				value |= readByte(false) << 8;
//				value |= readByte(false) << 16;
//				value |= readByte(false) << 24;
//				break;
//			}
//			return signed ? value : value & 0xffffffffL;
//		}
//
//		/**
//		 * Reads a signed standard big-endian integer.
//		 * 
//		 * @return the value
//		 */
//		public int readInt() {
//			return (int) readInt(true, ValueType.STANDARD, ByteOrder.BIG);
//		}
//
//		/**
//		 * Reads a standard big-endian integer.
//		 * 
//		 * @param signed
//		 *            the signedness
//		 * @return the value
//		 */
//		public long readInt(boolean signed) {
//			return readInt(signed, ValueType.STANDARD, ByteOrder.BIG);
//		}
//
//		/**
//		 * Reads a signed big-endian integer.
//		 * 
//		 * @param type
//		 *            the value type
//		 * @return the value
//		 */
//		public int readInt(ValueType type) {
//			return (int) readInt(true, type, ByteOrder.BIG);
//		}
//
//		/**
//		 * Reads a big-endian integer.
//		 * 
//		 * @param signed
//		 *            the signedness
//		 * @param type
//		 *            the value type
//		 * @return the value
//		 */
//		public long readInt(boolean signed, ValueType type) {
//			return readInt(signed, type, ByteOrder.BIG);
//		}
//
//		/**
//		 * Reads a signed standard integer.
//		 * 
//		 * @param order
//		 *            the byte order
//		 * @return the value
//		 */
//		public int readInt(ByteOrder order) {
//			return (int) readInt(true, ValueType.STANDARD, order);
//		}
//
//		/**
//		 * Reads a standard integer.
//		 * 
//		 * @param signed
//		 *            the signedness
//		 * @param order
//		 *            the byte order
//		 * @return the value
//		 */
//		public long readInt(boolean signed, ByteOrder order) {
//			return readInt(signed, ValueType.STANDARD, order);
//		}
//
//		/**
//		 * Reads a signed integer.
//		 * 
//		 * @param type
//		 *            the value type
//		 * @param order
//		 *            the byte order
//		 * @return the value
//		 */
//		public int readInt(ValueType type, ByteOrder order) {
//			return (int) readInt(true, type, order);
//		}
//
//		/**
//		 * Reads a signed long value.
//		 * 
//		 * @param type
//		 *            the value type
//		 * @param order
//		 *            the byte order
//		 * @return the value
//		 */
//		public long readLong(ValueType type, ByteOrder order) {
//			long value = 0;
//			switch (order) {
//			case BIG:
//				value |= (long) readByte(false) << 56L;
//				value |= (long) readByte(false) << 48L;
//				value |= (long) readByte(false) << 40L;
//				value |= (long) readByte(false) << 32L;
//				value |= (long) readByte(false) << 24L;
//				value |= (long) readByte(false) << 16L;
//				value |= (long) readByte(false) << 8L;
//				value |= readByte(false, type);
//				break;
//			case MIDDLE:
//				throw new UnsupportedOperationException(
//						"middle-endian long is not implemented!");
//			case INVERSE_MIDDLE:
//				throw new UnsupportedOperationException(
//						"inverse-middle-endian long is not implemented!");
//			case LITTLE:
//				value |= readByte(false, type);
//				value |= (long) readByte(false) << 8L;
//				value |= (long) readByte(false) << 16L;
//				value |= (long) readByte(false) << 24L;
//				value |= (long) readByte(false) << 32L;
//				value |= (long) readByte(false) << 40L;
//				value |= (long) readByte(false) << 48L;
//				value |= (long) readByte(false) << 56L;
//				break;
//			}
//			return value;
//		}
//
//		/**
//		 * Reads a signed standard big-endian long.
//		 * 
//		 * @return the value
//		 */
//		public long readLong() {
//			return readLong(ValueType.STANDARD, ByteOrder.BIG);
//		}
//
//		/**
//		 * Reads a signed big-endian long
//		 * 
//		 * @param type
//		 *            the value type
//		 * @return the value
//		 */
//		public long readLong(ValueType type) {
//			return readLong(type, ByteOrder.BIG);
//		}
//
//		/**
//		 * Reads a signed standard long.
//		 * 
//		 * @param order
//		 *            the byte order
//		 * @return the value
//		 */
//		public long readLong(ByteOrder order) {
//			return readLong(ValueType.STANDARD, order);
//		}
//
//		/**
//		 * Reads a RuneScape string value.
//		 * 
//		 * @return the string
//		 */
//		public String readString() {
//			byte temp;
//			StringBuilder b = new StringBuilder();
//			while ((temp = (byte) readByte()) != 10) {
//				b.append((char) temp);
//			}
//			return b.toString();
//		}
//
//		/**
//		 * Reads the amuont of bytes into the array, starting at the current
//		 * position.
//		 * 
//		 * @param amount
//		 *            the amount to read
//		 * @return a buffer filled with the data
//		 */
//		public byte[] readBytes(int amount) {
//			return readBytes(amount, ValueType.STANDARD);
//		}
//
//		/**
//		 * Reads the amount of bytes into a byte array, starting at the current
//		 * position.
//		 * 
//		 * @param amount
//		 *            the amount of bytes
//		 * @param type
//		 *            the value type of each byte
//		 * @return a buffer filled with the data
//		 */
//		public byte[] readBytes(int amount, ValueType type) {
//			byte[] data = new byte[amount];
//			for (int i = 0; i < amount; i++) {
//				data[i] = (byte) readByte(type);
//			}
//			return data;
//		}
//
//		/**
//		 * Reads the amount of bytes from the buffer in reverse, starting at
//		 * current position + amount and reading in reverse until the current
//		 * position.
//		 * 
//		 * @param amount
//		 *            the amount of bytes
//		 * @param type
//		 *            the value type of each byte
//		 * @return a buffer filled with the data
//		 */
//		public byte[] readBytesReverse(int amount, ValueType type) {
//			byte[] data = new byte[amount];
//			int dataPosition = 0;
//			for (int i = buffer.position() + amount - 1; i >= buffer.position(); i--) {
//				int value = buffer.get(i);
//				switch (type) {
//				case A:
//					value -= 128;
//					break;
//				case C:
//					value = -value;
//					break;
//				case S:
//					value = 128 - value;
//					break;
//				}
//				data[dataPosition++] = (byte) value;
//			}
//			return data;
//		}
//
//		/**
//		 * Gets the internal buffer.
//		 * 
//		 * @return the buffer
//		 */
//		public ByteBuffer getBuffer() {
//			return buffer;
//		}
//
//	}
//
//	/**
//	 * A StreamBuffer used to write outgoing data.
//	 * 
//	 * @author blakeman8192
//	 */
//	public static final class OutBuffer extends Buffer {
//
//		/** The internal buffer. */
//		private ByteBuffer buffer;
//
//		/** The position of the packet length in the packet header. */
//		private int lengthPosition = 0;
//
//		/**
//		 * Creates a new OutBuffer.
//		 * 
//		 * @param size
//		 *            the size
//		 */
//		private OutBuffer(int size) {
//			buffer = ByteBuffer.allocate(size);
//		}
//
//		@Override
//		void switchAccessType(AccessType type) {
//			switch (type) {
//			case BIT_ACCESS:
//				setBitPosition(buffer.position() * 8);
//				break;
//			case BYTE_ACCESS:
//				buffer.position((getBitPosition() + 7) / 8);
//				break;
//			}
//		}
//
//		/**
//		 * Writes a packet header.
//		 * 
//		 * @param cipher
//		 *            the encryptor
//		 * @param value
//		 *            the value
//		 */
//		public void writeHeader(int value) {
//			writeByte(value);
//		}
//
//		/**
//		 * Writes a packet header for a variable length packet. Note that the
//		 * corresponding "finishVariablePacketHeader" must be called to finish
//		 * the packet.
//		 * 
//		 * @param cipher
//		 *            the ISAACCipher encryptor
//		 * @param value
//		 *            the value
//		 */
//		public void writeVariablePacketHeader(int value) {
//			writeHeader(value);
//			lengthPosition = buffer.position();
//			writeByte(0);
//		}
//
//		/**
//		 * Writes a packet header for a variable length packet, where the length
//		 * is written as a short instead of a byte. Note that the corresponding
//		 * "finishVariableShortPacketHeader must be called to finish the packet.
//		 * 
//		 * @param cipher
//		 *            the ISAACCipher encryptor
//		 * @param value
//		 *            the value
//		 */
//		public void writeVariableShortPacketHeader(int value) {
//			writeHeader(value);
//			lengthPosition = buffer.position();
//			writeShort(0);
//		}
//
//		/**
//		 * Finishes a variable packet header by writing the actual packet length
//		 * at the length byte's position. Call this when the construction of the
//		 * actual variable length packet is complete.
//		 */
//		public void finishVariablePacketHeader() {
//			buffer.put(lengthPosition, (byte) (buffer.position()
//					- lengthPosition - 1));
//		}
//
//		/**
//		 * Finishes a variable packet header by writing the actual packet length
//		 * at the length short's position. Call this when the construction of
//		 * the variable length packet is complete.
//		 */
//		public void finishVariableShortPacketHeader() {
//			buffer.putShort(lengthPosition, (short) (buffer.position()
//					- lengthPosition - 2));
//		}
//
//		/**
//		 * Writes the bytes from the argued buffer into this buffer. This method
//		 * does not modify the argued buffer, and please do not flip() the
//		 * buffer before hand.
//		 * 
//		 * @param from
//		 */
//		public void writeBytes(ByteBuffer from) {
//			for (int i = 0; i < from.position(); i++) {
//				writeByte(from.get(i));
//			}
//		}
//
//		/**
//		 * Writes the bytes into this buffer.
//		 * 
//		 * @param from
//		 */
//		public void writeBytes(byte[] from, int size) {
//			buffer.put(from, 0, size);
//		}
//
//		/**
//		 * Writes the bytes from the argued byte array into this buffer, in
//		 * reverse.
//		 * 
//		 * @param data
//		 *            the data to write
//		 */
//		public void writeBytesReverse(byte[] data) {
//			for (int i = data.length - 1; i >= 0; i--) {
//				writeByte(data[i]);
//			}
//		}
//
//		/**
//		 * Writes the value as a variable amount of bits.
//		 * 
//		 * @param amount
//		 *            the amount of bits
//		 * @param value
//		 *            the value
//		 */
//		public void writeBits(int amount, int value) {
//			if (getAccessType() != AccessType.BIT_ACCESS) {
//				throw new IllegalStateException("Illegal access type.");
//			}
//			if (amount < 0 || amount > 32) {
//				throw new IllegalArgumentException(
//						"Number of bits must be between 1 and 32 inclusive.");
//			}
//
//			int bytePos = getBitPosition() >> 3;
//			int bitOffset = 8 - (getBitPosition() & 7);
//			setBitPosition(getBitPosition() + amount);
//
//			// Re-size the buffer if need be.
//			int requiredSpace = bytePos - buffer.position() + 1;
//			requiredSpace += (amount + 7) / 8;
//			if (buffer.remaining() < requiredSpace) {
//				ByteBuffer old = buffer;
//				buffer = ByteBuffer.allocate(old.capacity() + requiredSpace);
//				old.flip();
//				buffer.put(old);
//			}
//
//			for (; amount > bitOffset; bitOffset = 8) {
//				byte tmp = buffer.get(bytePos);
//				tmp &= ~BIT_MASK[bitOffset];
//				tmp |= value >> amount - bitOffset & BIT_MASK[bitOffset];
//				buffer.put(bytePos++, tmp);
//				amount -= bitOffset;
//			}
//			if (amount == bitOffset) {
//				byte tmp = buffer.get(bytePos);
//				tmp &= ~BIT_MASK[bitOffset];
//				tmp |= value & BIT_MASK[bitOffset];
//				buffer.put(bytePos, tmp);
//			} else {
//				byte tmp = buffer.get(bytePos);
//				tmp &= ~(BIT_MASK[amount] << bitOffset - amount);
//				tmp |= (value & BIT_MASK[amount]) << bitOffset - amount;
//				buffer.put(bytePos, tmp);
//			}
//		}
//
//		/**
//		 * Writes a boolean bit flag.
//		 * 
//		 * @param flag
//		 *            the flag
//		 */
//		public void writeBit(boolean flag) {
//			writeBits(1, flag ? 1 : 0);
//		}
//
//		/**
//		 * Writes a value as a byte.
//		 * 
//		 * @param value
//		 *            the value
//		 * @param type
//		 *            the value type
//		 */
//		public void writeByte(int value, ValueType type) {
//			if (getAccessType() != AccessType.BYTE_ACCESS) {
//				throw new IllegalStateException("Illegal access type.");
//			}
//			switch (type) {
//			case A:
//				value += 128;
//				break;
//			case C:
//				value = -value;
//				break;
//			case S:
//				value = 128 - value;
//				break;
//			}
//			buffer.put((byte) value);
//		}
//
//		/**
//		 * Writes a value as a normal byte.
//		 * 
//		 * @param value
//		 *            the value
//		 */
//		public void writeByte(int value) {
//			writeByte(value, ValueType.STANDARD);
//		}
//
//		/**
//		 * Writes a value as a short.
//		 * 
//		 * @param value
//		 *            the value
//		 * @param type
//		 *            the value type
//		 * @param order
//		 *            the byte order
//		 */
//		public void writeShort(int value, ValueType type, ByteOrder order) {
//			switch (order) {
//			case BIG:
//				writeByte(value >> 8);
//				writeByte(value, type);
//				break;
//			case MIDDLE:
//				throw new IllegalArgumentException(
//						"Middle-endian short is impossible!");
//			case INVERSE_MIDDLE:
//				throw new IllegalArgumentException(
//						"Inverse-middle-endian short is impossible!");
//			case LITTLE:
//				writeByte(value, type);
//				writeByte(value >> 8);
//				break;
//			}
//		}
//
//		/**
//		 * Writes a value as a normal big-endian short.
//		 * 
//		 * @param value
//		 *            the value.
//		 */
//		public void writeShort(int value) {
//			writeShort(value, ValueType.STANDARD, ByteOrder.BIG);
//		}
//
//		/**
//		 * Writes a value as a big-endian short.
//		 * 
//		 * @param value
//		 *            the value
//		 * @param type
//		 *            the value type
//		 */
//		public void writeShort(int value, ValueType type) {
//			writeShort(value, type, ByteOrder.BIG);
//		}
//
//		/**
//		 * Writes a value as a standard short.
//		 * 
//		 * @param value
//		 *            the value
//		 * @param order
//		 *            the byte order
//		 */
//		public void writeShort(int value, ByteOrder order) {
//			writeShort(value, ValueType.STANDARD, order);
//		}
//
//		/**
//		 * Writes a value as an int.
//		 * 
//		 * @param value
//		 *            the value
//		 * @param type
//		 *            the value type
//		 * @param order
//		 *            the byte order
//		 */
//		public void writeInt(int value, ValueType type, ByteOrder order) {
//			switch (order) {
//			case BIG:
//				writeByte(value >> 24);
//				writeByte(value >> 16);
//				writeByte(value >> 8);
//				writeByte(value, type);
//				break;
//			case MIDDLE:
//				writeByte(value >> 8);
//				writeByte(value, type);
//				writeByte(value >> 24);
//				writeByte(value >> 16);
//				break;
//			case INVERSE_MIDDLE:
//				writeByte(value >> 16);
//				writeByte(value >> 24);
//				writeByte(value, type);
//				writeByte(value >> 8);
//				break;
//			case LITTLE:
//				writeByte(value, type);
//				writeByte(value >> 8);
//				writeByte(value >> 16);
//				writeByte(value >> 24);
//				break;
//			}
//		}
//
//		/**
//		 * Writes a value as a standard big-endian int.
//		 * 
//		 * @param value
//		 *            the value
//		 */
//		public void writeInt(int value) {
//			writeInt(value, ValueType.STANDARD, ByteOrder.BIG);
//		}
//
//		/**
//		 * Writes a value as a big-endian int.
//		 * 
//		 * @param value
//		 *            the value
//		 * @param type
//		 *            the value type
//		 */
//		public void writeInt(int value, ValueType type) {
//			writeInt(value, type, ByteOrder.BIG);
//		}
//
//		/**
//		 * Writes a value as a standard int.
//		 * 
//		 * @param value
//		 *            the value
//		 * @param order
//		 *            the byte order
//		 */
//		public void writeInt(int value, ByteOrder order) {
//			writeInt(value, ValueType.STANDARD, order);
//		}
//
//		/**
//		 * Writes a value as a long.
//		 * 
//		 * @param value
//		 *            the value
//		 * @param type
//		 *            the value type
//		 * @param order
//		 *            the byte order
//		 */
//		public void writeLong(long value, ValueType type, ByteOrder order) {
//			switch (order) {
//			case BIG:
//				writeByte((int) (value >> 56));
//				writeByte((int) (value >> 48));
//				writeByte((int) (value >> 40));
//				writeByte((int) (value >> 32));
//				writeByte((int) (value >> 24));
//				writeByte((int) (value >> 16));
//				writeByte((int) (value >> 8));
//				writeByte((int) value, type);
//				break;
//			case MIDDLE:
//				throw new UnsupportedOperationException(
//						"Middle-endian long is not implemented!");
//			case INVERSE_MIDDLE:
//				throw new UnsupportedOperationException(
//						"Inverse-middle-endian long is not implemented!");
//			case LITTLE:
//				writeByte((int) value, type);
//				writeByte((int) (value >> 8));
//				writeByte((int) (value >> 16));
//				writeByte((int) (value >> 24));
//				writeByte((int) (value >> 32));
//				writeByte((int) (value >> 40));
//				writeByte((int) (value >> 48));
//				writeByte((int) (value >> 56));
//				break;
//			}
//		}
//
//		/**
//		 * Writes a value as a standard big-endian long.
//		 * 
//		 * @param value
//		 *            the value
//		 */
//		public void writeLong(long value) {
//			writeLong(value, ValueType.STANDARD, ByteOrder.BIG);
//		}
//
//		/**
//		 * Writes a value as a big-endian long.
//		 * 
//		 * @param value
//		 *            the value
//		 * @param type
//		 *            the value type
//		 */
//		public void writeLong(long value, ValueType type) {
//			writeLong(value, type, ByteOrder.BIG);
//		}
//
//		/**
//		 * Writes a value as a standard long.
//		 * 
//		 * @param value
//		 *            the value
//		 * @param order
//		 *            the byte order
//		 */
//		public void writeLong(long value, ByteOrder order) {
//			writeLong(value, ValueType.STANDARD, order);
//		}
//
//		/**
//		 * Writes a RuneScape string value.
//		 * 
//		 * @param string
//		 *            the string
//		 */
//		public void writeString(String string) {
//			for (byte value : string.getBytes()) {
//				writeByte(value);
//			}
//			writeByte(10);
//		}
//
//		/**
//		 * Gets the internal buffer.
//		 * 
//		 * @return the buffer
//		 */
//		public ByteBuffer getBuffer() {
//			return buffer;
//		}
//
//	}
}