package client;

import java.awt.BorderLayout;
import java.lang.reflect.InvocationTargetException;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.SwingUtilities;

import client.network.Network;
import client.network.packet.out.LoginPacket;

public class Client extends JPanel {
	
	public static Client client;
	public static LoginPanel loginPanel;
	public static UserPanel userPanel;
	
	private Client() {
		super(new BorderLayout());
		loginPanel = new LoginPanel(this);
		userPanel = new UserPanel(this);
		super.add(loginPanel, BorderLayout.CENTER);
	}

	public void requestLogin(String string) {
		Network.start();
		Network.send(new LoginPacket(string));
	}
	
	public void login() {
		super.removeAll();
		super.add(userPanel, BorderLayout.CENTER);
		super.revalidate();
		super.repaint();
	}
	
	public void logout() {
		super.removeAll();
		super.add(loginPanel, BorderLayout.CENTER);
		super.revalidate();
		super.repaint();
	}
	
	public void removeComp(int idx) {
		userPanel.removeComp(idx);
	}
	
	public static void main(String[] args) throws InvocationTargetException, InterruptedException {
		
		SwingUtilities.invokeAndWait(new Runnable() {
    		public void run() {
    			JFrame frame = new JFrame();
    			
    			client = new Client();
    	    	
    			frame.setContentPane(client);
    			frame.setSize(170, 200);
    			frame.setResizable(false);
    			frame.setLocationRelativeTo(null);
    			frame.setVisible(true);
    		}
    	});
	}

}
