package client.network;
  
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.Socket;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.LinkedBlockingQueue;

import client.network.packet.in.InPacket;
import client.network.packet.out.OutPacket;
import shared.ThreadStarter;

public class Connection {

    private Socket socket;

    private LinkedBlockingQueue<OutPacket> packetQueueOut = new LinkedBlockingQueue<OutPacket>();
    private LinkedBlockingQueue<InPacket> packetQueueIn = new LinkedBlockingQueue<InPacket>();

    private CountDownLatch latch;

    public Connection(Socket socket) throws IOException {
        this.socket = socket;

        latch = new CountDownLatch(3);

        new Sender(socket.getOutputStream());
        new Receiver(socket.getInputStream());
        new PacketHandler();

        try {
            latch.await();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    private class Sender implements Runnable {

        private OutputStream outputStream;

        private Sender(OutputStream outputStream) throws IOException {
            this.outputStream = outputStream;
            ThreadStarter.startRunnable(this, 3);
        }

        public void run() {
            latch.countDown();
            while (true) {
                try {
                    packetQueueOut.take().send(outputStream);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    private class Receiver implements Runnable {

        private InputStream inputStream;

        private Receiver(InputStream inputStream) throws IOException {
            this.inputStream = inputStream;
            ThreadStarter.startRunnable(this, 3);
        }

        public void run() {
            latch.countDown();
            while (true) {
                try {
                    byte opcode = (byte) inputStream.read();
                    if (opcode == -1)
                    	continue;

                    InPacket packet = InPacket.getServerPacket(opcode);
                    
                    packet.receive(inputStream);

                    packet.print();
                    packetQueueIn.put(packet);
                } catch (IOException e) {
                    finish();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    private class PacketHandler implements Runnable {
        private PacketHandler() {
            ThreadStarter.startRunnable(this, 3);
        }

        @Override
        public void run() {
            latch.countDown();
            while (true) {
                try {
                    packetQueueIn.take().process();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    public void finish() {
        try {
            socket.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void send(OutPacket packet) {
        packetQueueOut.add(packet);
    }
}