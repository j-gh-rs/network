package client.network;

import java.util.Arrays;

public abstract class Packet extends shared.io.Buffer {

	protected final byte opcode;
    
    public Packet(int opcode, int length) {
    	super(length);
		this.opcode = (byte) opcode;
	}
    
    public Packet(int opcode) {
		this.opcode = (byte) opcode;
	}
 
    public void print() {
        System.out.println(this.getClass().getName()+" (opcode "+opcode+"): "+Arrays.toString(data));
    }
}
