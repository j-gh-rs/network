package client.network.packet.in;

import java.io.IOException;
import java.io.InputStream;

import client.network.Packet;
 
public abstract class InPacket extends Packet {
 
    public static InPacket getServerPacket(byte opcode) {
//        if (opcode == -1)
//            return null;
        switch (opcode) {
//        case ServerToClient.LOGGING_IN: return new LoginPacket();
//        case ServerToClient.PLAYER_INITIALIZE: return new PlayerInitializePacket();
//        case ServerToClient.PLAYERS_UPDATE: return new PlayersUpdatePacket();
//        case ServerToClient.MOBS_UPDATE: return new MobsUpdatePacket();
//        case ServerToClient.UPDATE_MAP_REGION: return new MapRegionPacket();
        case 54: return new LoginResponsePacket();
        case 55: return new TestResponsePacket();
        }
        System.err.println("Unable to find packet for opcode: "+opcode);
        return null;
    }
 
    protected InPacket(int opcode, int length) {
    	super(opcode, length);
    }
     
    public void process() {};
     
    public void receive(InputStream inputStream) {
//    	if (data == null)
//    		return;
        try {
            inputStream.read(data);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
