package client.network.packet.in;

import client.Client;

public class TestResponsePacket extends InPacket {

	public TestResponsePacket() {
		super(55, 1);
	}
	
    @Override
    public void process() {
    	System.out.println("PROCESSING");
    	
    	int location = super.readUInt8();
    	Client.client.removeComp(location);
    }
    
}