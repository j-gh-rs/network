package client.network.packet.in;

import client.Client;
import log.Log;

public class LoginResponsePacket extends InPacket {

	public LoginResponsePacket() {
		super(54, 30);
	}
	
    @Override
    public void process() {
    	int id = super.readInt16();
    	if (id == -1) {
    		Log.log("Server full");
    		return;
    	}
    	Client.userPanel.updateInventory(this);
    	Client.client.login();
    }
}
