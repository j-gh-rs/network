package client.network.packet.out;

public class LoginPacket extends VarPacket {
	
	public LoginPacket(String username) {
		super(54, username.length());
		super.write(username.getBytes());
	}
	
	@Override
	public void build() {
	}
}