package client.network.packet.out;

import java.io.IOException;
import java.io.OutputStream;
import java.util.Arrays;
 
/** Class representing variable length packet **/
 
public abstract class VarPacket extends OutPacket {
 
    protected VarPacket(int opcode) {
        super(opcode);
    }
 
    protected VarPacket(int opcode, int maxLength) {
        super(opcode, maxLength);
    }
 
    @Override
    public void send(OutputStream outputStream) {
        try {
            outputStream.write(opcode);
            outputStream.write(position());
            outputStream.write(data, 0, position());
            System.out.println("Sending: "+Arrays.toString(data));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
 
    public void setLength(int length) {
    	data = new byte[length];
    }
}
