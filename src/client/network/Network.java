package client.network;
 
import java.io.IOException;
import java.net.InetAddress;
import java.net.Socket;

import client.network.packet.out.OutPacket;
 
public class Network {
     
    private static Connection connection;
     
    public static void start() {
        try {
            Socket socket = new Socket(InetAddress.getByName(shared.Constants.HOST), shared.Constants.PORT);
//          socket.setSoTimeout(30000);
            socket.setTcpNoDelay(true);
            connection = new Connection(socket);
        } catch (IOException e) {
            e.printStackTrace();
            System.out.println("Unable to start network!");
        }
    }
 
    public static void send(OutPacket packet) {
        connection.send(packet);
    }
}
