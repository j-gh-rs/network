package client;

import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JPanel;
import javax.swing.JTextField;

public class LoginPanel extends JPanel {
	
	public LoginPanel(Client client) {
		super(new BorderLayout());
		JTextField field = new JTextField();
		JButton button = new JButton("Log in");
		button.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				client.requestLogin(field.getText());
			}
		});
		super.add(field, BorderLayout.CENTER);
		super.add(button, BorderLayout.SOUTH);
	}

}
