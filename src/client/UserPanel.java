package client;

import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;

import client.network.Network;
import client.network.packet.out.OutPacket;
import client.network.packet.out.TestPacket;
import shared.io.Buffer;

public class UserPanel extends JPanel implements Runnable {
	
	private final JPanel topPanel;
	
	public UserPanel(Client client) {
		super(new BorderLayout());
		topPanel = new JPanel(new GridLayout(7, 4));
		
		JButton button = new JButton("Log out");
		button.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				client.logout();
			}
		});
		super.add(topPanel, BorderLayout.CENTER);
		super.add(button, BorderLayout.SOUTH);
    	
//    	for (int i = 0; i < 28; i++) {
//    		Component c;
//    		if (i % 1 == 0) {
//    			final int idxFinal = i;
//    			c = new PacketButton(new TestPacket() {
//    				@Override
//    				public void build() {
//    					super.writeInt8(idxFinal);
//    					super.writeInt8((byte) Math.max(0, (int)(Math.random()*5-2)));
//    				}
//    			});
//			} else {
//	    		c = new JLabel();
//			}
//    		topPanel.add(c);
//    		topPanel.revalidate();
//    		topPanel.repaint();
//    	}
	}

	@Override
	public void run() {
//		int idx = -1;
//		while (idx < 28 && !(topPanel.getComponent(++idx) instanceof JLabel));
//		if (idx == 28)
//			return;
//		System.out.println("Adding packet");
//		final int idxFinal = idx;
//		topPanel.remove(idx);
//		topPanel.add(new PacketButton(new TestPacket() {
//			@Override
//			public void build() {
//				super.writeInt8(idxFinal);
//				super.writeInt8((byte) Math.max(0, (int)(Math.random()*5-2)));
//			}
//		}), idx);
//		topPanel.revalidate();
//		topPanel.repaint();
	}
	
	private class PacketButton extends JButton {
		private PacketButton(OutPacket packet) {
			super(packet.toString());
			
			super.addActionListener(new ActionListener() {
				@Override
				public void actionPerformed(ActionEvent e) {
					Network.send(packet);
				}
			});
		}
	}

	public void removeComp(int idx) {
		topPanel.remove(idx);
		topPanel.add(new JLabel(), idx);
		topPanel.revalidate();
		topPanel.repaint();
	}

	public void updateInventory(Buffer buffer) {
		for (int i = 0; i < 28; i++) {
			final int tmp = i;
			final int ticks = buffer.readUInt8();
			PacketButton button = new PacketButton(new TestPacket() {
				@Override
				public void build() {
					super.writeInt8(tmp);
					super.writeInt8(ticks);
				}
			});
			topPanel.add(button);
		}
		topPanel.revalidate();
		topPanel.repaint();
	}

}
