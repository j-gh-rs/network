package server.network;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.Socket;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.LinkedBlockingQueue;

import server.Player;
import server.network.packet.in.InPacket;
import server.network.packet.out.OutPacket;
import shared.ThreadStarter;

public class Connection {

	private final Socket socket;
	
	private final LinkedBlockingQueue<OutPacket> packetQueueOut = new LinkedBlockingQueue<OutPacket>();
	private final LinkedBlockingQueue<InPacket> packetQueueIn = new LinkedBlockingQueue<InPacket>();

	private final OutputStream outputStream;
	
	public Player player;
	
	private CountDownLatch latch;
	
	private final Sender sender;

	public Connection(Socket socket) throws IOException {
		this.socket = socket;
		
		outputStream = socket.getOutputStream();

		latch = new CountDownLatch(2);
		sender = new Sender(socket.getOutputStream());
		new Receiver(socket.getInputStream());

		try {
			latch.await();
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}

	private class Sender implements Runnable {

		private OutputStream outputStream;

		private Sender(OutputStream outputStream) throws IOException {
			this.outputStream = outputStream;
//			ThreadStarter.startRunnable(this, 3);
			latch.countDown();
		}

		public void run() {
			while (!packetQueueOut.isEmpty()) {
				try {
					packetQueueOut.take().send(outputStream);
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
			}
		}
	}

	private class Receiver implements Runnable {

		private InputStream inputStream;

		private Receiver(InputStream inputStream) throws IOException {
			this.inputStream = inputStream;
			ThreadStarter.startRunnable(this, 3);
		}

		public void run() {
			latch.countDown();
			while (true) {
				try {
					byte opcode = (byte) inputStream.read();

                    InPacket packet = InPacket.getClientPacket(Connection.this, opcode);
                    packet.receive(inputStream);

                    packet.print();
                    packetQueueIn.put(packet);
				} catch (IOException e) {
					close();
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
			}
		}
	}

	public void close() {
		try {
			socket.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

    public void queue(OutPacket packet) {
        packetQueueOut.add(packet);
    }

	public void processPackets() {
		for (InPacket p = packetQueueIn.poll(); p != null; p = packetQueueIn.poll()) {
			p.process();
		}
	}

	public void sendPackets() {
		for (OutPacket p = packetQueueOut.poll(); p != null; p = packetQueueOut.poll()) {
			p.send(outputStream);
		}
	}
}
