package server.network;

import java.util.Arrays;

public abstract class Packet extends shared.io.Buffer {

	protected byte opcode;
    
    public Packet(int opcode, int length) {
    	super(length);
		this.opcode = (byte) opcode;
	}
    
    public Packet(byte[] data) {
		super(data);
	}
 
    public void print() {
        System.out.println(this.getClass().getName()+" (opcode "+opcode+"): "+Arrays.toString(data));
    }
}