package server.network;

import java.io.IOException;
import java.net.ServerSocket;
import java.util.concurrent.LinkedBlockingQueue;

import log.Log;

public class Network {
	
	public static final LinkedBlockingQueue<Connection> PENDING_CONNECTIONS = new LinkedBlockingQueue<Connection>();
	
	private static ServerSocket serverSocket;
	
	public static void start() {
		try {
			serverSocket = new ServerSocket(shared.Constants.PORT);
			new Thread() {
				public void run() {
					while (true) {
						try {
						    PENDING_CONNECTIONS.put(new Connection(serverSocket.accept()));
						    Log.log("NEW CONNECTION!");
						} catch (IOException e) {
							e.printStackTrace();
						} catch (InterruptedException e) {
							e.printStackTrace();
						}
					}
				}
			}.start();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}