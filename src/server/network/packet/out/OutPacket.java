package server.network.packet.out;

import java.io.IOException;
import java.io.OutputStream;
import java.util.Arrays;

import client.network.Packet;
 
public abstract class OutPacket extends Packet {
 
    protected OutPacket(int opcode, int length) {
    	super(opcode, length);
        build();
    }
 
    public void send(OutputStream outputStream) {
        try {
        	System.out.println("WRITE OPCODE");
            outputStream.write(opcode);
        	System.out.println("WRITE DATA");
            outputStream.write(data, 0, position());
            System.out.println("Sending: "+Arrays.toString(data));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    
    abstract public void build();
}
