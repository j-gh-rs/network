package server.network.packet.out;

import server.Player;

public class LoginResponsePacket extends OutPacket {
	
	public LoginResponsePacket(Player player) {
		super(54, 30);
		super.writeInt16(player == null ? -1 : player.id);
		if (player != null)
			for (int i = 0; i < 28; i++)
				super.writeInt8((byte) Math.max(0, (int)(Math.random()*5-2)));
	}
	
	@Override
	public void build() {
		
	}
}