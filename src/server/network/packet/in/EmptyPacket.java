package server.network.packet.in;

public class EmptyPacket extends InPacket {
    
    public EmptyPacket(int opcode) {
        super(opcode, 0);
    }
 
    @Override
    public void process() {}
}