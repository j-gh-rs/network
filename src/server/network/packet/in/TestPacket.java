package server.network.packet.in;

import server.network.Connection;
import server.network.packet.out.TestResponsePacket;

public class TestPacket extends InPacket {
	
	private final Connection c;

	public TestPacket(Connection c) {
		super(55, 2);
		this.c = c;
	}
	
    @Override
    public void process() {
    	
    	int location = super.readUInt8();
    	int ticks = super.readUInt8();
    	
    	if (c.player.ticks > 0) {
    		System.out.println("IGNORED");
    		return;
    	}
    	
    	c.player.ticks = ticks;
    	
    	TestResponsePacket packet = new TestResponsePacket() {
    		@Override
    		public void build() {
    			super.writeInt8(location);
    		}
    	};
    	
    	c.queue(packet);
    }
    
}