package server.network.packet.in;

import java.io.IOException;
import java.io.InputStream;

import server.network.Connection;
import server.network.Packet;
 
public abstract class InPacket extends Packet {
 
    public static InPacket getClientPacket(Connection c, byte opcode) {
        if (opcode == -1)
            return null;
        switch (opcode) {
        case 54: return new LoginPacket(c);
        case 55: return new TestPacket(c);
        }
        System.err.println("Unable to find packet for opcode: "+opcode);
        return null;
    }
 
    protected InPacket(int opcode, int length) {
        super(opcode, length);
    }
     
    public void process() {};
     
    public void receive(InputStream inputStream) {
    	if (data == null)
    		return;
        try {
            inputStream.read(data);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
