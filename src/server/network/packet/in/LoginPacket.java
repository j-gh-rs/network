package server.network.packet.in;

import server.network.Connection;
import server.network.packet.out.LoginResponsePacket;

public class LoginPacket extends VarPacket {
	
	private final Connection c;

	public LoginPacket(Connection c) {
		super(54);
		this.c = c;
	}
	
    @Override
    public void process() {
    	c.player.name = new String(super.data);
    	LoginResponsePacket packet = new LoginResponsePacket(c.player);
    	c.queue(packet);
    }
    
}