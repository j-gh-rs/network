package server.network.packet.in;

import java.io.IOException;
import java.io.InputStream;
 
/** Class representing variable length packet **/
 
public abstract class VarPacket extends InPacket {
	
	private int n; /** number of bytes to read to get array length **/
 
    protected VarPacket(int opcode) {
    	this(opcode, 1);
    }
    
    protected VarPacket(int opcode, int n) {
        super(opcode, 0);
        this.n = n;
    }
     
    @Override
    public void receive(InputStream inputStream) {
        try {
        	byte[] b = new byte[n];
        	inputStream.read(b);
        	int length = 0;
        	for (int i = 0; i < n; i++) {
        		length <<= 8;
        		length |= b[i];
        	}
            data = new byte[length];
            super.receive(inputStream);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
