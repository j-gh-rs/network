package server;

import server.network.Connection;

public class Player {
	
	private final Connection c;
	public final int id;
	public String name;
	
	public int ticks;

	public Player(int id, Connection c) {
		this.id = id;
		this.c = c;
		c.player = this;
	}

	public void processPackets() {
		c.processPackets();
	}

	public void sendPackets() {
		c.sendPackets();
	}
	
	public void loop() {
		if (ticks > 0) {
			ticks--;
		}
	}

}
