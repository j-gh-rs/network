package server;

import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

import server.network.Connection;
import server.network.Network;

public class Server implements Runnable {

	public static final int TICK = 3000;
	public static final int MAX_USERS = 1;
	public static final Player[] USERS = new Player[MAX_USERS];
	
	public Server() {
		Network.start();
		
		ScheduledExecutorService executor = Executors.newScheduledThreadPool(1);
		executor.scheduleAtFixedRate(this, 0, TICK, TimeUnit.MILLISECONDS);
	}
	
	public static void main(String[] args) {
		new Server();
	}

	@Override
	public void run() {
		System.out.println("Loop");
		
		/** Add new users */
		for (Connection c = Network.PENDING_CONNECTIONS.poll(); c != null; c = Network.PENDING_CONNECTIONS.poll())
			if (addUser(c) == null)
				c.close();
		
		/** Process incoming packets */
		for (int i = 0; i < USERS.length; i++) {
			Player u  = USERS[i];
			if (u == null)
				continue;
			u.processPackets();
		}
		
		/** Do game logic */
		for (int i = 0; i < USERS.length; i++) {
			Player u  = USERS[i];
			if (u == null)
				continue;
			u.loop();
		}
		
		/** Send outgoing packets */
		for (int i = 0; i < USERS.length; i++) {
			Player u  = USERS[i];
			if (u == null)
				continue;
			u.sendPackets();
		}
	}

	public static Player addUser(Connection c) {
		for (int i = 0; i < USERS.length; i++) {
			if (USERS[i] == null) {
				USERS[i] = new Player(i, c);
				return USERS[i];
			}
		}
		return null;
	}

}
